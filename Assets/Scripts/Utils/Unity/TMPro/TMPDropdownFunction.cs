﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using Utils;
using System;

public class TMPDropdownFunction : ExtensionBehaviour<TMP_Dropdown>
{
  public string OptionTemplate = string.Empty;
  public string OptionFormatString = string.Empty;
  public SerializableNumberFormatInfo OptionFormat
    = new SerializableNumberFormatInfo();

  public TMPDropdownFunction SetFormattedInts(IEnumerable<int> values)
  {
    BaseComp.options.Clear();
    values.Select(v => new TMP_Dropdown.OptionData(FormatNumber(v)))
      .PassTo(BaseComp.options.AddRange);
    return this;
  }

  string FormatNumber(int number) => string.Format(OptionTemplate,
    number.ToString(OptionFormatString, OptionFormat));
}
