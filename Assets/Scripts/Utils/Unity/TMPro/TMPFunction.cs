﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Utils;
using MyBox;

public class TMPFunction : ExtensionBehaviour<TextMeshPro>
{
  public string Template = "{0}";
  public string FormatString = string.Empty;
  public SerializableNumberFormatInfo FormatInfo
    = new SerializableNumberFormatInfo();
  int _currentBoundInt = 0;
  public float IntChangePerSecond = 0;

  public void SetInt(int number) => BaseComp.text = string.Format(Template,
    number.ToString(FormatString, FormatInfo));

  public void SetInt(object number) => SetInt((int)number);

  public void ChangeTo(int newInt) => StartCoroutine(UpdateText(newInt));

  public void ChangeTo(object newInt) => ChangeTo((int)newInt);

  public void SetBoundInt(int number) => _currentBoundInt = number;

  IEnumerator UpdateText(int endValue)
  {
    var totalWait = _currentBoundInt.Difference(endValue) / IntChangePerSecond;
    var secondsElapsed = 0f;
    do
    {
      Mathf.Lerp(_currentBoundInt, endValue, secondsElapsed / totalWait)
        .PassTo(Mathf.RoundToInt)
        .PassTo(SetInt);
      yield return Utils.Unity.Constants.WaitForFixedUpdate;
      secondsElapsed += Time.fixedDeltaTime;
    } while (secondsElapsed < totalWait);
    _currentBoundInt = endValue;
    SetInt(endValue);
    yield break;
  }
}
