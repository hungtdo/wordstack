﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using TMPro;

[DisallowMultipleComponent]
[RequireComponent(typeof(TMP_InputField))]
public class ValidIfTextLengthInRange : ClientsideValidator<TMP_InputField>
{
  [SerializeField] int MinValidLength = 6;
  [SerializeField] int MaxValidLength = 15;

  void Start() { }

  public override bool Validate() =>
    BaseComp.text.Length.InRange(MinValidLength, MaxValidLength);
}
