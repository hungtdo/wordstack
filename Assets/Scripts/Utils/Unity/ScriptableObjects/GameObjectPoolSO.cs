﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/GameObjectPoolSO")]
public class GameObjectPoolSO : ScriptableObject
{
  public GameObjectPool Value = null; 
}
