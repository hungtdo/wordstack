﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using System.Linq;
using System;

namespace WordStack.Stacking
{
  public class WordSettingsInstance : MonoBehaviour
  {
    public WordSettings Settings = null;
    public Transform ComposedZone = null;

    void Awake() { Settings.ComposedZone = ComposedZone; }
  }
}
