﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Linq;

namespace WordStack.Stacking
{
  [Serializable]
  public class UnityEventKnobs : UnityEvent<IEnumerable<Knob.Knob>> { }
}
