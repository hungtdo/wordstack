﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using System.Linq;
using System;
using Lean.Transition;
using WordStack.Stacking.Knob;

namespace WordStack.Stacking
{
  [CreateAssetMenu(menuName = "ScriptableObjects/WordStack/Stacking/WordSettings")]
  public class WordSettings : ScriptableObject
  {
    public TextAsset WordDictionary = null;
    [MinMaxRange(1, 20)] public RangedInt WordLengthRange = new RangedInt();
    [HideInInspector] public string[] ValidWords = new string[0];
    public SerializableDictionaryCharInt CharFrequencies
      = new SerializableDictionaryCharInt();
    static readonly public string Alphabet = "abcdefghijklmnopqrstuvwxyz";

    public Transform ComposedZone = null;
    [ReadOnly]
    public List<Knob.CloneMode> ComposedClones = new List<Knob.CloneMode>();
    public EventSO OnInWordLengthRange = null;
    public EventSO OnMaxWordLengthReached = null;
    public EventSO OnOutOfWordLengthRange = null;
    public EventSO OnRemoveAll = null;

    void Awake()
    {
      ValidWords = WordDictionary.text
        .Split(new[] { Environment.NewLine }, StringSplitOptions.None)
        .Select(line => line.Trim())
        .Where(line => line.OnlyCharactersIn(WordSettings.Alphabet))
        .Where(line => line.Length.InRange(WordLengthRange))
        .Distinct().ToArray();
      ValidWords.SelectMany(g => g.GroupBy(c => c))
        .GroupBy(g => g.Key, g => g.Count())
        .OrderBy(g => g.Key)
        .ForEach(g => CharFrequencies[g.Key] = g.Sum());
    }


    void OnDisable()
    {
      ComposedClones.Clear();
    }

    public void Compose(Knob.CloneMode newKnobClone)
    {
      ComposedClones.Add(newKnobClone);
      if (ComposedClones.Count == WordLengthRange.Min)
        OnInWordLengthRange.Listeners();
      if (ComposedClones.Count == WordLengthRange.Max)
        OnMaxWordLengthReached.Listeners();
      Rearrange();
    }

    public void Remove(Knob.CloneMode removedKnobClone)
    {
      OnInWordLengthRange.Listeners();
      ComposedClones.Remove(removedKnobClone);
      if (ComposedClones.Count < WordLengthRange.Min)
        OnOutOfWordLengthRange.Listeners();
      Rearrange();
    }

    public void Rearrange()
    {
      var leftEdgeX = ComposedClones.Sum(c => -c.BaseComp.Size.Value.Scale / 2);
      float totalOffsetFromEdge = 0f;
      ComposedClones.ForEach(clone =>
      {
        var position = clone.BaseComp.Size.Value.Scale.Multiply(0.5f)
          .Plus(totalOffsetFromEdge + leftEdgeX)
          .Multiply(Vector3.right)
          .Plus(ComposedZone.position);
        clone.transform.positionTransition(position, 0.25f,
          LeanEase.Decelerate);
        totalOffsetFromEdge += clone.BaseComp.Size.Value.Scale;
      });
    }

    public void ClearClones()
    {
      OnInWordLengthRange.Listeners();
      OnOutOfWordLengthRange.Listeners();
      ComposedClones.Clear();
    }

    public void RemoveAll()
    {
      if (ComposedClones.Count == 0) return;
      OnRemoveAll.Listeners();
      ComposedClones.ForEach(clone => clone.ReturnToOriginal());
      ClearClones();
    }

    public string ComposedWord => ComposedClones
      .Select(c => c.BaseComp.Text.text)
      .MakeTupleAppend(string.Empty)
      .PassTo(string.Join).ToLower();

    public bool ComposedWordValid => ValidWords.Contains(ComposedWord);

    public void DestroyCloneOriginals()
    {
      ComposedClones.ForEach(c =>
      {
        c.Original.gameObject.SetActive(false);
        c.gameObject.SetActive(false);
        Destroy(c);
      });
      ClearClones();
    }
  }
}
