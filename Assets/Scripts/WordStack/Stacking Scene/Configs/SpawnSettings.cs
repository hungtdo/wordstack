﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WordStack.Stacking
{
  [CreateAssetMenu(menuName = "ScriptableObjects/WordStack/Stacking/SpawnSettings")]
  public class SpawnSettings : ScriptableObject
  {
    public Collider2D SummonZone = null;
    public Knob.Pool KnobPool = null;
    public Knob.SizeSettings[] KnobSizes = new Knob.SizeSettings[0];
    public ColorHDRSettings[] KnobMaterialColors = new ColorHDRSettings[0];
    public int SpawningNumber = 0;
    public float SecondsBetweenSpawns = 0f;
    public VariableBool IsLidActive = null;
    public VariableFloat RoundLength = null;
    public VariableFloat SecondsTilNextRound = null;
  }
}
