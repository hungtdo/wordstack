﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using System.Linq;

namespace WordStack.Stacking
{
  [CreateAssetMenu(menuName = "ScriptableObjects/WordStack/Stacking/Scene")]
  public class Configs : ScriptableObject
  {
    [Foldout("Settings")] public WordSettings WordSettings = null;
    [Foldout("Settings")] public SpawnSettings SpawnSettings = null;
    [Foldout("Settings")] public ScoreSettings ScoreSettings = null;
    public EventSO OnBeginKnobSpawn = null;
    public EventSO OnKnobSpawnDone = null;
    public EventSO OnAllKnobsAsleep = null;
    public Knob.EventKnobsSO OnComposedWordValid = null;
    public EventSO OnComposedWordInvalid = null;
    public EventSO OnNewRound = null;
    public EventSO OnReset = null;

    public void ToggleOriginalKnobsDarkOverlay(bool state) => SpawnSettings
      .KnobPool.Instances.Where(i => i.gameObject.activeSelf)
      .Except(WordSettings.ComposedClones.Select(c => c.Original))
      .Except(WordSettings.ComposedClones.Select(c => c.BaseComp))
      .Select(go => go.GetComponent<Knob.Knob>())
      .ForEach(k => k.ToggleDarkLayer(state));

    public void EnableKnobsClonable() => SpawnSettings.KnobPool.Instances
      .Where(i => i.gameObject.activeSelf)
      .Except(WordSettings.ComposedClones.Select(c => c.Original))
      .Except(WordSettings.ComposedClones.Select(c => c.BaseComp))
      .ForEach(k => k.gameObject
        .GetOrAddComponent<Knob.CloneOnMouseUpAsButton>());

    public void DisableKnobsClonable() => SpawnSettings.KnobPool.Instances
      .Select(i => i.GetComponent<Knob.CloneOnMouseUpAsButton>())
      .Where(c => c != null)
      .ForEach(Destroy);

    public void CheckWordValidity()
    {
      if (WordSettings.ComposedWordValid) WordSettings.ComposedClones
        .Select(c => c.Original)
        .PassTo(OnComposedWordValid.Invoke);
      else OnComposedWordInvalid.Invoke();
    }

    public void EmitSparks(IEnumerable<Knob.Knob> knobs) => knobs.ForEach(k =>
    {
      var knobPS = k.Settings.SparkPSPool.Value
        .GetFirst(ps => ps.isStopped);
      knobPS.transform.position = knobPS.transform.position.SetXY(
        k.transform.position.x,
        k.transform.position.y);
      knobPS.gameObject.SetActive(true);
      var particleEmissionRange = k.Size.Value.ParticlesEmittedOnPop;
      Random.Range(particleEmissionRange.Min, particleEmissionRange.Max)
        .PassTo(knobPS.Emit);
    });

    public void ExplodeKnobs(IEnumerable<Knob.Knob> knobs) => knobs
      .ForEach(k => k.AudioEmitter.PlayClip(k.Settings.ExplosionClip));

    public void ConvertComposedWordToScore() => ScoreSettings.Score.Value +=
      WordSettings.ComposedClones.Select(c => c.BaseComp)
        .GroupBy(k => k.Color.Value)
        .GroupBy(g => g.Key, g => Enumerable.Range(0, g.Count())
          .Select(mod => mod * ScoreSettings.SameColorModifier)
          .Select(mod => mod + 1f)
          .Zip(g, (mul, k) => ScoreSettings.BaseKnobScore
            .Multiply(k.Size.Value.ScoreMultiplier)
            .Multiply(mul)
            .Round())
          .Sum())
        .Sum(g => g.First());
  }
}
