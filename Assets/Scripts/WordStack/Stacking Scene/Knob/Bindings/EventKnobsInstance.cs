﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WordStack.Stacking.Knob
{
  public class EventKnobsInstance : MonoBehaviour
  {
    public EventKnobsSO Source = null;
    public UnityEventKnobs Event = new UnityEventKnobs();

    void Awake() => Source.Listeners += Event.Invoke;

    void OnDestroy() => Source.Listeners += Event.Invoke;
  }
}
