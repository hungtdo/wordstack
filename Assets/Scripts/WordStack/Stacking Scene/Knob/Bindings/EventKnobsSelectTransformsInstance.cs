﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace WordStack.Stacking.Knob
{
  public class EventKnobsSelectTransformsInstance : MonoBehaviour
  {
    public EventKnobsSO Source = null;
    public UnityEventTransforms Event = new UnityEventTransforms();

    void Awake() => Source.Listeners += Select;

    void OnDestroy() => Source.Listeners += Select;

    void Select(IEnumerable<Knob> knobs) => knobs.Select(k => k.transform);
  }
}
