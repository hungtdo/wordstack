﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

namespace WordStack.Stacking.Knob
{
  [CreateAssetMenu(menuName = "ScriptableObjects/WordStack/Stacking/Knob/SharedSettings")]
  public class SharedSettings : ScriptableObject
  {
    [Layer] public int DefaultLayer = 0;
    [Layer] public int CloneModeLayer = 0;
    public ParticleSystemPoolSO SparkPSPool = null;
    public AudioClip PickClip = null;
    public AudioClip UnpickClip = null;
    public AudioClip ExplosionClip = null;
  }
}
