﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Utils;
using Lean.Transition;
using UnityEngine.EventSystems;
using System.Linq;

namespace WordStack.Stacking.Knob
{
  public class CloneOnMouseUpAsButton : RuntimeExtensionBehaviour<Knob>
  {
    void OnMouseUpAsButton()
    {
      if (EventSystem.current.IsPointerOverGameObject()) return;
      var firstInactive = BaseComp.SceneConfigs.SpawnSettings.KnobPool
        .GetFirst(k => !k.gameObject.activeSelf);
      firstInactive.gameObject.SetActive(true);
      var clone = firstInactive.GetOrAddComponent<CloneMode>().Init(BaseComp);
      clone.BaseComp.Sprite.material.SetAlpha(0f);
      clone.BaseComp.Sprite.material.colorTransition("_BaseColor",
        clone.BaseComp.Sprite.material.color.WithAlphaSetTo(1f),
        0.25f,
        LeanEase.Accelerate);
      BaseComp.SceneConfigs.WordSettings.Compose(clone);
      BaseComp.AudioEmitter.PlayClip(BaseComp.Settings.PickClip);
      clone.GetComponent<CloneOnMouseUpAsButton>()
        ?.PassTo(UnityEngine.Object.Destroy);
      Destroy(this);
    }
  }
}
