﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Linq;
using Utils;

namespace WordStack.Menu
{
  public class HighscoreBoard : MonoBehaviour
  {
    public XMLDataFileSO HighscoreFile = null;
    public int TopScoreNumber = 10;
    public string ScoreQuery = string.Empty;
    public string ScoreTimeQuery = string.Empty;
    public Transform HighscoreContainer = null;
    public ScoreItem ScoreItemPrefab = null;
    public List<ScoreItem> ScoreItems = new List<ScoreItem>();

    void Start()
    {
      var timestamps = HighscoreFile.Document.SelectNodes(ScoreTimeQuery)
        .Cast<XmlAttribute>()
        .Select(a => a.Value);
      HighscoreFile.Document.SelectNodes(ScoreQuery).Cast<XmlAttribute>()
        .Select(a => a.Value)
        .Zip(timestamps, (s, t) => (Score: s, Time: t))
        .OrderByDescending(scoreInfo => scoreInfo.Score.ToInt())
        .Take(TopScoreNumber)
        .Zip(Enumerable.Range(1, TopScoreNumber),
          (scoreInfo, rank) =>
          {
            var newItem = Instantiate(ScoreItemPrefab, HighscoreContainer);
            newItem.RankText.text = $"#{rank.ToString()}";
            newItem.ScoreText.text = scoreInfo.Score;
            newItem.TimestampText.text = scoreInfo.Time;
            return newItem;
          })
        .PassTo(ScoreItems.AddRange);
    }
  }
}
